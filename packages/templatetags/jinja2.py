from urllib import urlencode, quote as urlquote, unquote
from django.utils.html import escape
from django_jinja import library
from main.templatetags import pgp


@library.filter
def url_unquote(original_url):
    try:
        url = original_url
        if isinstance(url, unicode):
            url = url.encode('ascii')
        url = unquote(url).decode('utf-8')
        return url
    except UnicodeError:
        return original_url


def link_encode(url, query):
    # massage the data into all utf-8 encoded strings first, so urlencode
    # doesn't barf at the data we pass it
    query = {k: unicode(v).encode('utf-8') for k, v in query.items()}
    data = urlencode(query)
    return "%s?%s" % (url, data)


@library.global_function
def pgp_key_link(key_id, link_text=None):
    return pgp.pgp_key_link(key_id, link_text)


@library.global_function
def scm_link(package, operation):
    parts = (package.repo.name.lower(), operation, package.pkgbase)
    linkbase = (
        "https://git.hyperbola.info:50100/packages/%s.git/%s/%s")
    return linkbase % tuple(urlquote(part.encode('utf-8')) for part in parts)


@library.global_function
def wiki_link(package):
    url = "https://wiki.hyperbola.info/doku.php"
    data = {
        'do': "search",
        'id': package.pkgname,
    }
    return link_encode(url, data)

@library.global_function
def bugs_list(package):
    project = "issue-tracker"
    url = "https://issues.hyperbola.info/projects/%s/search" % project
    data = {
        'titles_only': '1',
        'issues': '1',
        'q': package.pkgname,
    }
    return link_encode(url, data)


@library.global_function
def bug_report(package):
    url = "https://issues.hyperbola.info/projects/"
    url = url + "issue-tracker/issues/new"
    data = {
        'issue[subject]': '[%s] PLEASE ENTER SUMMARY' % package.pkgname,
    }
    return link_encode(url, data)

@library.global_function
def flag_nonfree(package):
    url = "https://issues.hyperbola.info/projects/"
    url = url + "issue-tracker/issues/new"
    data = {
        'issue[tracker_id]': '4', # "freedom issue"
        'issue[priority_id]': '1', # "freedom issue"
        'issue[watcher_user_ids][]': '62', # "dev-list"
        'issue[subject]': '[%s] Please put your reasons here (register first if you haven\'t)' % package.pkgname,
    }
    return link_encode(url, data)

# vim: set ts=4 sw=4 et:
