def secure(request):
    return {'secure': request.is_secure()}

def branding(request):
    from django.conf import settings
    return {
        'BUGTRACKER_URL': settings.BUGTRACKER_URL,
        'MAILMAN_BASE_URL': settings.MAILMAN_BASE_URL,
        'PROJECTS_URL': settings.PROJECTS_URL,

        'BRANDING_APPNAME': settings.BRANDING_APPNAME,
        'BRANDING_DISTRONAME': settings.BRANDING_DISTRONAME,
        'BRANDING_GNUPLUSLINUXNAME': settings.BRANDING_GNUPLUSLINUXNAME,
        'BRANDING_GNUPLUSHURDNAME': settings.BRANDING_GNUPLUSHURDNAME,
        'BRANDING_WIKINAME': settings.BRANDING_WIKINAME,
        'BRANDING_EMAIL': settings.BRANDING_EMAIL,
        'BRANDING_OSEARCH_TAGS': settings.BRANDING_OSEARCH_TAGS,
    }

# vim: set ts=4 sw=4 et:
