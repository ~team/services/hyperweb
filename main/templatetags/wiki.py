from django import template
register = template.Library()

@register.simple_tag
def wiki_url(article=""):
    if article == "":
        return "https://wiki.hyperbola.info/"
    else:
        return "https://wiki.hyperbola.info/"+article.replace(' ', '_')
